Log
===

Aplus Framework Log Library.

- `Installation`_

Installation
------------

The installation of this library can be done with Composer:

.. code-block::

    composer require aplus/log
